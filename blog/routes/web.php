<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});
Route::get('/register', 'RegisterController@register');
// Route::get('/welcome', 'RegisterController@welcome');
Route::post('/welcome', 'RegisterController@welcome_post');
Route::get('/master', function () {
    return view('adminlite.master');
});
Route::get('/table', function () {
    return view('adminlite.content.table');
});
Route::get('/datatable', function () {
    return view('adminlite.content.datatable');
});

//CRUD
Route::get('/cast/create', 'CastController@create');

Route::post('/cast', 'CastController@store');

Route::get('/cast', 'CastController@index');

Route::get('/cast/{cast_id}', 'CastController@show');

Route::get('/cast/{cast_id}/edit', 'CastController@edit');

Route::put('/cast/{cast_id}', 'CastController@update');

Route::delete('/cast/{cast_id}', 'CastController@destroy');
