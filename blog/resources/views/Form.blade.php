<!DOCTYPE html>
<html>
<header>
    <title>Buat Account Baru</title>
</header>

<body>
    <form action="/welcome" method="POST" class="justify-center">
        @csrf
        <table>
            <tr>
                <td>
                    <label>First Name</label>
                </td>
                <td>
                    <input type="text" name="namaAwal">
                </td>
            </tr>
            <tr>
                <td>
                    <label>Last Name</label>
                </td>
                <td>
                    <input type="text" name="namaAkhir">
                </td>
            </tr>
            <tr>
                <td>
                    <label>Gender</label>
                </td>
                <td>
                    <input type="radio" name="sex" />Male
                    <input type="radio" name="sex" />Female
                </td>
            </tr>
            <tr>
                <td>
                    <label>Nationality</label>
                </td>
                <td>
                    <select>
                        <option value="Amerika Serikat">Amerika Serikat</option>
                        <option value="China">China</option>
                        <option value="England">England</option>
                        <option value="Germany">Germany</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Singapura">Singapura</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Language Spoken</label>
                </td>
                <td>
                    <input type="checkbox" />English
                    <input type="checkbox" />Mandarin
                    <input type="checkbox" />Bahasa Indonesia
                    <input type="checkbox" />Others : <input type="text" size="10" maxlength="20" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Bio</label>
                </td>
                <td>
                    <textarea name="Bio" cols="30" rows="10"></textarea>
                </td>
            </tr>
        </table>
        <input type="submit">
    </form>
</body>