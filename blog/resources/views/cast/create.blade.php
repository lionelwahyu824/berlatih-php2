@extends('adminlite.master')
@section('tab-judul')
CREATE Cast
@endsection
@section('content')
<!-- general form elements -->
<div class="card card-secondary">
    <div class="card-header">
        <h3 class="card-title">Create New Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/cast" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', '') }}" placeholder="Masukkan Nama" required>
                @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur', '') }}" placeholder="Masukkan Umur" required>
                @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', '') }}" placeholder="Masukkan Bio" required>
                @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-secondary">Create</button>
        </div>
    </form>
</div>
<!-- /.card -->
@endsection