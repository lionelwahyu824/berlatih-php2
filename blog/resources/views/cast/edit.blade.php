@extends('adminlite.master')

@section('tab-judul')
EDIT Cast
@endsection

@section('content')
<!-- general form elements -->
<div class="card card-secondary">
    <div class="card-header">
        <h3 class="card-title">Edit Cast {{ $showcast->id }}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/cast/{{ $showcast->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', $showcast->nama) }}" placeholder="Masukkan Nama" required>
                @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur', $showcast->umur) }}" placeholder="Masukkan Umur" required>
                @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', $showcast->bio) }}" placeholder="Masukkan Bio" required>
                @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-secondary">Edit</button>
        </div>
    </form>
</div>
<!-- /.card -->
@endsection