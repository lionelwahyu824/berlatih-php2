@extends('adminlite.master')

@section('tab-judul')
INDEX ID Cast
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Detail Cast {{ $showcast->id}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr align="center">
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $showcast->nama }}</td>
                    <td>{{ $showcast->umur }}</td>
                    <td>{{ $showcast->bio }}</td>
                </tr>
                {{-- <tr>
                <td>1.</td>
                <td>Update software</td>
                <td>Update software</td>
                <td>
                <div class="progress progress-xs">
                    <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                </div>
                </td>
                <td><span class="badge bg-danger">55%</span></td>
            </tr> --}}
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection