@extends('adminlite.master')

@section('tab-judul')
INDEX Cast
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Cast Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(session('success'))
        <div class="alert alert-light alert-dismissible fade show" role="alert">
            {{ session('success') }}
        </div>
        @endif
        <a class="btn btn-info mb-3" href="/cast/create">Create New Cast</a>
        <table class="table table-bordered">
            <thead>
                <tr align="center">
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 170px">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key => $cast)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $cast->nama }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>{{ $cast->bio }}</td>
                    <td style="display: flex">
                        <a href="/cast/{{$cast->id}}" class="btn btn-success btn-sm">show</a>
                        <a href="/cast/{{$cast->id}}/edit" class="btn btn-primary btn-sm">edit</a>
                        <form action="/cast/{{$cast->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" align="center">No Cast</td>
                </tr>
                @endforelse
                {{-- <tr>
                <td>1.</td>
                <td>Update software</td>
                <td>Update software</td>
                <td>
                <div class="progress progress-xs">
                    <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                </div>
                </td>
                <td><span class="badge bg-danger">55%</span></td>
            </tr> --}}
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    {{-- <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
        </ul>
        </div> --}}
</div>
@endsection