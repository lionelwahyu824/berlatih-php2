<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register()
    {
        return view('form');
    }

    // public function welcome(Request $request)
    // {
    //     // var_dump($request);
    //     dd($request);
    //     return view('welcome');
    // }

    public function welcome_post(Request $request)
    {
        //dd($request->all());
        $nama = $request["namaAwal"];
        $nama2 = $request["namaAkhir"];
        return view('welcome', ["nama" => $nama], ["nama2" => $nama2]);
    }
}
